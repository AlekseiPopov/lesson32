import { Box } from "@material-ui/core";
import Typography from '@material-ui/core/Typography';

function RandomUser(props) {
    return (
        <>
            <Box>
                <Typography variant="body2" color="textSecondary" align="center">
                    Случайный пользователь от BackEnd (https://randomuser.me/api/):
                </Typography>
                <Typography variant="body2" color="textSecondary" align="center">
                    {props.FirstName}
                </Typography>
                <Typography variant="body2" color="textSecondary" align="center">
                    {props.LastName}
                </Typography>
            </Box>
        </>
    );
}

export default RandomUser;